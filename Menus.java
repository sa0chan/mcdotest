public class Menus {

    protected Drinks  drink;
    protected Burger hamburger;
    protected Vegetables vegetable  ;
    protected Double price ;

    public Menus(Drinks boisson, Burger burger,Vegetables legumes){
        drink = boisson;
        hamburger = burger ;
        vegetable = legumes ;
    }

    public Drinks getDrink() {
        return drink;
    }

    public Burger getHamburger() {
        return hamburger;
    }

    public Vegetables getVegetable() {
        return vegetable;
    }

    public double getPrice() {
        price =  5.0 ;
        return price ;
    }

    @Override
    public String toString() {
        return " contient {" +
                " une boisson moyenne='" + drink + '\'' +
                ", un hamburger='" + hamburger + '\'' +
                ", un accompagnement moyen='" + vegetable + '\'' +
                '}';
    }
}
