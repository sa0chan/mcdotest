import java.util.Random;

public class HappyMeal extends Menus {

    private static final Toys[] VALUES = Toys.values();
    private static final int SIZE = VALUES.length;
    private static final Random RANDOM = new Random();
    Toys toy ;


    public HappyMeal(Drinks boisson, Burger burger,Vegetables legumes){
        super( boisson, burger, legumes);

    }

    public Toys getRandomToy() {
        toy =  VALUES[RANDOM.nextInt(SIZE)];
        return toy ;
    }

    @Override
    public double getPrice() {
        price =  3.5;
        return price ;
    }

    @Override
    public String toString() {
        this.getRandomToy();
        return "HappyMeal qui contient {" +
                " un jouet =" + toy +
                ", une boisson='" + drink + '\'' +
                ", un hamburger='" + hamburger + '\'' +
                ", un accompagnement='" + vegetable + '\'' +
                '}';
    }
}
