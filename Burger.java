public enum Burger {
    BIGMAC("Big Mac"),
    FILETOFISH("Filet O Fish");

    private String burgerName ;

    //Constructeur
    Burger(String nameBurger){
        this.burgerName = nameBurger;
    }
    //si on ne redefini pas toString il garde le toString de la Class Objet qui est la super Class de toutes les classes . pas besoin de le mettre à la fin d'une instance car i le fait directement
    public String toString(){
        return burgerName;
    }

}