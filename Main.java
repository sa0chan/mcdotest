import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Menus bestOfBigMac = new Menus(Drinks.COCA,Burger.BIGMAC,Vegetables.POTATOES);
        Menus bestOfFish = new Menus(Drinks.EAU,Burger.FILETOFISH,Vegetables.SALAD);
        Maxi maxiBestOfFish = new Maxi(Drinks.FANTA,Burger.FILETOFISH,Vegetables.FRENCHFRIES);
        HappyMeal happyMeal = new HappyMeal(Drinks.FANTA,Burger.BIGMAC,Vegetables.FRENCHFRIES);
        HappyMeal happyMeal2 = new HappyMeal(Drinks.EAU,Burger.BIGMAC,Vegetables.POTATOES);


        List<Menus> listeCommande1 = new ArrayList<>();
        listeCommande1.add(bestOfFish);
        listeCommande1.add(bestOfBigMac);
        listeCommande1.add(maxiBestOfFish);
        listeCommande1.add(happyMeal);
        listeCommande1.add(happyMeal2);

        ArrayList<Double> totalAmount = new ArrayList<>() ;


        int i = 1 ;
        System.out.println("Votre commande contient " + listeCommande1.size() + " menus :  " );
        for (Menus menu: listeCommande1) {

        System.out.println( " menu " + i + " : "+ menu.toString());
        System.out.println( " Il coute " + menu.getPrice() + " euros");
        totalAmount.add(menu.getPrice());

        i++;

        }

        Double sumMenuAmount =0.00 ;
        for (Double menuPrice :totalAmount) {
            sumMenuAmount = sumMenuAmount + menuPrice  ;
        }

        System.out.println( "Le total de la commande est de  " + sumMenuAmount + " euros");


    }
}
