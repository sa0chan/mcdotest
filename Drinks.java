public enum Drinks {

        COCA("Coca Cola"),
        FANTA("Fanta"),
        EAU("Eau");

        private String drinkName ;

        //Constructeur
        Drinks(String nameDrink){
            this.drinkName = nameDrink;
        }

        public String toString(){
            return drinkName;
        }


}
