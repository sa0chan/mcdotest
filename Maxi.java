import java.awt.*;

public class Maxi extends Menus {

    public Maxi(Drinks grandeBoisson, Burger grandBurger,Vegetables grandLegumes){
        super( grandeBoisson,  grandBurger, grandLegumes) ;

    }

    @Override
    public double getPrice() {
        price =  5.5;
        return price ;
    }

    public String toString() {

        return "contient {" +
                " une Grande Boisson='" + drink + '\'' +
                ", un hamburger='" + hamburger + '\'' +
                ", une Grand accompagnement ='" + vegetable + '\'' +
                '}';
    }

}
