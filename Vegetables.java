public enum Vegetables {

    FRENCHFRIES("Frites"),
    POTATOES("Potatoes"),
    SALAD("Salade");

    private String VegetableName ;

    //Constructeur
    Vegetables(String nameVegetable){
        this.VegetableName = nameVegetable;
    }

    public String toString(){
        return VegetableName;
    }
}
