public enum Toys {

    TOY1("jeu de societe"),
    TOY2("autocollant"),
    TOY3("figurines"),
    TOY4("billes"),
    TOY5("crayons"),
    TOY6("voitures");

    private String toyName ;

    //Constructeur
    Toys(String nameToy){
        this.toyName = nameToy;
    }

    public String toString(){
        return toyName;
    }
}
